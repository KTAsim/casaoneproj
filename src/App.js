import React, {useEffect} from 'react';
import './App.css';
import AppSection from './molecules/AppSection';
import UserAddresses from './molecules/UserAddresses';
import UserProducts from './molecules/UserProducts';
import { useStateValue } from './utils/state-handler';

function App() {
  const [{apiLoaded}, dispatch] = useStateValue();
  const fetchApi = async () => {
    const data = await fetch('https://demo4030603.mockable.io/data');
    const dataJSON = await data.json();
    console.log("Store: ",dataJSON);
    dispatch({
      type: 'API_LOAD',
      newState: dataJSON
    })
  }
  useEffect(() => {
    !apiLoaded && fetchApi();
  })
  return (
    <div className="App">
      <AppSection>
       <UserAddresses />      
      </AppSection>
      <AppSection>
        <UserProducts />
      </AppSection>
    </div>
  );
}

export default App;
