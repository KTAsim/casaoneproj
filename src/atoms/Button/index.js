import React from 'react';
import PropTypes from 'prop-types';
import './Button.css';
const Button = ({ children="Submit", className="", classNameSuffix="", ...props}) => (
  <button className={`Button ${className} Button-${classNameSuffix.toLowerCase()}`} {...props}>{children}</button>
)
Button.propTypes = {
    children: PropTypes.node,
    className: PropTypes.string
};
export default Button;
