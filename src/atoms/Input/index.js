import React from 'react';
import PropTypes from 'prop-types';
import './Input.css';
const Input = ({ type="text", className="", ...props }) => (
  type==='textarea' ? <textarea  className={`Input ${className}`} {...props} placeholder = { props.placeholder || props.name } ></textarea> :
  <input type={type} className={`Input ${className}`} placeholder = { props.placeholder || props.name } {...props}/>
)
Input.propTypes = {
    placeholder: PropTypes.string,
    name: PropTypes.string,
    type: PropTypes.string
};
export default Input;
