import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import { StateProvider } from './utils/state-handler';
import reducer from './utils/reducer'
const initialState = {
    checkout: {
        addresses: {
            billing: {},
            shipping: {}
        },
        order_date: '04/09/2019',
        expected_delivery_date: '15/10/2019'
    },
    products: [
    ],
    error: '',
    apiLoaded: false
}

const AppWithStore = () => <StateProvider state={initialState} reducer={reducer}><App /></StateProvider>
ReactDOM.render(<AppWithStore />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
