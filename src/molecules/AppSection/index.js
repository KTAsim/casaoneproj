import React from 'react';
import PropTypes from 'prop-types';
import './AppSection.css';
const AppSection = ({ children }) => (
    children ? <div className="AppSection">{children}</div> : null
)
AppSection.propTypes = {
    children: PropTypes.node.isRequired
};
export default AppSection;
