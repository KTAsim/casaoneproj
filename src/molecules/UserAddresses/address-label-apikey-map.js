export const labelApiMap = [
    {
        label: 'First Name',
        api_key: 'first_name' 
    },
    {
        label: 'Last Name',
        api_key: 'last_name' 
    },
    {
        label: 'Address Line 1',
        api_key: 'address_1'
    },
    {
        label: 'Address Line 2',
        api_key: 'address_2'
    },
    {
        label: 'City',
        api_key: 'city'
    },
    {
        label: 'State',
        api_key: 'state'
    },
    {
        label: 'Zipcode',
        api_key: 'zip'
    },{
        label: 'Country',
        api_key: 'country'
    }
]