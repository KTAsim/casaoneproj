import React from 'react';
import './UserAddresses.css';
import Input from '../../atoms/Input';
import { useStateValue } from '../../utils/state-handler';
import get from 'lodash.get';
import { labelApiMap } from './address-label-apikey-map';


const UserAddresses = () => {
    const [{ checkout }] = useStateValue();
    const billingAddress = get(checkout, ['addresses', 'billing'], {});
    const shippingAddress = get(checkout, ['addresses', 'shipping'], {});
    const orderDate = {
        title: 'Order Date',
        date: get(checkout, ['order_date'], "")
    }
    const expectedDate = {
        title: 'Expected Delivery',
        date: get(checkout, ['expected_delivery_date'], "")
    }
    return <div className="UserAddresses">
        <AddressColoumn columnTitle="Billing Address" address={billingAddress} datePickerObj = {orderDate}/>
        <AddressColoumn columnTitle="Shipping Address" address={shippingAddress} datePickerObj = {expectedDate}/>
    </div>
}

const AddressColoumn = ({columnTitle = '', datePickerObj = '', address = {} }) => (
    <div className="AddressColoumn">
        <h3>{columnTitle}</h3>
        <form action="#">
        {
            address.address_1 && address.address_1.length
                ? labelApiMap.map(({label, api_key}, index) => 
                    <Input value={address[api_key]}
                        key={index}
                        placeholder={label}
                        name={label.replace(/\s/g,'') } 
                        className="AddressColoumn__input" 
                        readOnly/>)
                : <small>Loading the address...</small>

        }
        <h3>{datePickerObj.title}</h3>
        <Input type="date" className="AddressColoumn__input" value={datePickerObj.date} readOnly/>
        </form>
    </div>
)
export default UserAddresses;
