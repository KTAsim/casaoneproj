import React, { useState, useEffect } from 'react';
import './UserProducts.css';
import Input from '../../atoms/Input';
import Button from '../../atoms/Button';
import { useStateValue } from '../../utils/state-handler';

const AddProductForm = ({onFormSubmit = null}) => {
    // Store the new product until saving.
    const [newProduct, setProduct] = useState({})
    const [{error = '', products}, dispatch] = useStateValue();
    
    useEffect(() => {
      // Only clear out the form if there're no errors. 
      // As we can not directly check error (it'll be rewritten by the time) -
      // run this effect only products are changed.
      if(Object.keys(newProduct).length !== 0) {
        // Clearing out current form. And closing the add product row.
        setProduct({});
        onFormSubmit();
      }
    }, [products])
    const handleInputChange = (event) => {
      const target = event.target;
      const value = target.type === 'checkbox' ? target.checked : target.value;
      const name = target.name;
      setProduct({
        ...newProduct,
        [name]: isNaN(value) ? value : parseFloat(value)
      });
    }
    
    // Saving the product to the store.
    const updateProducts = (e) => {
      e.preventDefault();
      dispatch({
        type: 'PRODUCT_ADD',
        newProduct: newProduct
      })
    }
    const getTotalValue = () => {
      return newProduct.qty && newProduct.price ? newProduct.qty * newProduct.price : 0;
    }
    return <>
      <form className="UserProducts Wrapper--Desktop" onSubmit={updateProducts}>
        <Input type="number" className="UserProducts__input" required onChange={handleInputChange} name="id" />
        <Input className="UserProducts__input" required onChange={handleInputChange} name="name"/>
        {/* Support floating values upto two pointers for qty and price */}
        <Input type="number" className="UserProducts__input" required onChange={handleInputChange} name="qty" step="0.01"/>
        <Input type="number" className="UserProducts__input" required onChange={handleInputChange} name="price" step="0.01"/>
        <Input disabled className="UserProducts__input" required name="total" value={getTotalValue()} readOnly/>
        <Input type="textarea" className="UserProducts__input" onChange={handleInputChange} name="notes" />
        <Button classNameSuffix="normal" type="submit">Save</Button>
      </form>
      <form className="UserProducts" onSubmit={updateProducts}>
        <ul className="Wrapper--Mobile Wrapper--Form">
          <li><Input type="number" className="UserProducts__input" required onChange={handleInputChange} name="id" /></li>
          <li><Input className="UserProducts__input" required onChange={handleInputChange} name="name"/></li>
          {/* Support floating values upto two pointers for qty and price */}
          <li><Input type="number" className="UserProducts__input" required onChange={handleInputChange} name="qty" placeholder="quantity" step="0.01"/></li>
          <li><Input type="number" className="UserProducts__input" required onChange={handleInputChange} name="price" step="0.01"/></li>
          <li><Input disabled className="UserProducts__input" required name="total" value={getTotalValue()} readOnly/></li>
          <li><Input type="textarea" className="UserProducts__input" onChange={handleInputChange} name="notes" /></li>
        </ul>
        <Button classNameSuffix="normal" type="submit">Save</Button>
      </form>
      {error.length ? <p className="UserProducts__Error">{error}</p> : null}
    </>
  }
  
  export default AddProductForm;