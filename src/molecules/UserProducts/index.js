import React, { useState, useEffect } from 'react';
import './UserProducts.css';
import Input from '../../atoms/Input';
import Button from '../../atoms/Button';
import { useStateValue } from '../../utils/state-handler';
import AddProductForm from './AddProductForm';

const rowTitles = () => <>
  <p>Product Id</p>
  <p>Product Name</p>
  <p>Quantity</p>
  <p>Unit Price</p>
  <p>Total Price</p>
  <p>Notes</p>
  <p>Actions</p>
</>;
const UserProducts = () => {
  const [{ products, apiLoaded }, dispatch] = useStateValue();
  const deleteProduct = (id) => {
    dispatch({
      type: 'PRODUCT_DELETE',
      productId: id
    })
  }
  // Log the products only when products are changed.
  useEffect(() => console.log('The updated products are', products), [products]);
  const [showAddForm, setFormVisibility] = useState(false);
  return <>
    <div className="UserProducts Wrapper--Desktop">
      {rowTitles()}
      {
        products.length ? products.map(product => <React.Fragment key={product.id}>
        <Input type="number" value={product.id} className="UserProducts__input" required readOnly/>
        <Input value={product.name} className="UserProducts__input" required readOnly/>
        <Input type="number" value={product.qty} className="UserProducts__input" required readOnly/>
        <Input type="number" value={product.price} className="UserProducts__input" required readOnly/>
        <Input type="number" disabled value={product.qty * product.price} className="UserProducts__input" required readOnly/>
        <Input type="textarea" value={product.notes} className="UserProducts__input" required readOnly/>
        <Button classNameSuffix="danger" onClick={() => deleteProduct(product.id)}>Delete</Button>
        </React.Fragment>) : <small>{apiLoaded ? 'No products available' : 'Loading the products...'}<br/></small>
      }
    </div>
    <ul className="UserProducts Wrapper--Mobile">
      {
        products.length ? products.map(product => <React.Fragment key={product.id}>
        <li>Id: <Input type="number" value={product.id} className="UserProducts__input" required readOnly/></li>
        <li>Name: <Input value={product.name} className="UserProducts__input" required readOnly/></li>
        <li>Quantity: <Input type="number" value={product.qty} className="UserProducts__input" required readOnly/></li>
        <li>Price: <Input type="number" value={product.price} className="UserProducts__input" required readOnly/></li>
        <li>Total: <Input type="number" disabled value={product.qty * product.price} className="UserProducts__input" required readOnly/></li>
        <li>Notes: <Input type="textarea" value={product.notes} className="UserProducts__input" required readOnly/></li>
        <Button className="UserProducts__Button-mobile" classNameSuffix="danger" onClick={() => deleteProduct(product.id)}>Delete</Button>
        </React.Fragment>) : <small>{apiLoaded ? 'No products available' : 'Loading the products...'}<br/></small>
      }
    </ul>
    {showAddForm ?<AddProductForm onFormSubmit={() => setFormVisibility(false)}/> : null}
    <div className="UserProducts__ActionButtons">
      <Button classNameSuffix="normal" onClick={() => setFormVisibility(true)}>Add Product</Button>
    </div>
  </>
}

export default UserProducts;
