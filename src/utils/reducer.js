const reducer = (state = {}, action) => {
    switch (action.type) {
        case 'API_LOAD':
            return {
                ...state,
                ...action.newState,
                apiLoaded: true
            };
        case 'PRODUCT_ADD':
            const isProductAlreadyPresent = state.products.filter(product => product.id === action.newProduct.id)
            if(isProductAlreadyPresent.length) {
                return {
                    ...state,
                    error: 'The product id is same. Try with different product ID'
                }
            }
            return {
                ...state,
                error: '',
                products: [
                    ...state.products,
                    action.newProduct
                ]
            }
        case 'PRODUCT_DELETE':
            return {
                ...state,
                error: '',
                products: state.products.filter(product => product.id !== action.productId)
            }
        
        default:
            return state;
    }
};

export default reducer;